'use strict';


(function () {
  var $ = jQuery;

  $(document).ready(function() {
    initMobileMenu();
    initScrollTo();
    initSlider();
    initLoadMore();
    initPopup();
  });


  function initMobileMenu() {
    $('.main-menu > .main-menu__list').slicknav({
      label: '',
      prependTo: '.main-menu'
    });

    setMenuMaxHeight();

    window.addEventListener('orientationchange', function() {
      setMenuMaxHeight();
    });
  }

  function setMenuMaxHeight() {
    var viewportHeight = $(window).height();

    $('.slicknav_menu').css('maxHeight', viewportHeight);
  }

  function initScrollTo() {
    $('.main-menu__list').onePageNav({
      currentClass: 'main-menu__item--active'
    });
  }

  function initSlider() {
    var $sliderList = $('.slider__list');

    $sliderList.on('init', function() {
      var $firstAnimatingElements = $('.slider__item:first-child').find('[data-animation]');

      doAnimations($firstAnimatingElements);
    });

    $sliderList.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
      var $animatingElements = $('.slider__item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');

      doAnimations($animatingElements);
    });

    $sliderList.slick({
      appendDots: '.slider__indicators',
      arrows: false,
      dots: true,
      fade: true
    });

    function doAnimations($elements) {
      var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

      $elements.each(function() {
        var $this = $(this);
        var $animationDelay = $this.data('delay');
        var $animationType = 'animated ' + $this.data('animation');

        $this.css({
          'animation-delay': $animationDelay,
          '-webkit-animation-delay': $animationDelay
        });

        $this.addClass($animationType).one(animationEndEvents, function() {
          $this.removeClass($animationType);
        });
      });
    }
  }

  function initLoadMore() {
    $('#show-more').click(function () {
      $('.works').addClass('works--expanded');
      $('#show-more').addClass('hidden');
    });
  }

  function initPopup() {
    $('.contact-form').submit(function (e) {
      e.preventDefault();

      $.magnificPopup.open({
        items: {
          src: '#contact-form-popup',
          type: 'inline'
        }
      });
    });
  }
}());

// Initializes after the script loading
function initGoogleMap() {
  var ffw = { lat: 48.4557461, lng: 35.0501678 };
  var map = new google.maps.Map(document.querySelector('.map'), { zoom: 17, center: ffw });
  var marker = new google.maps.Marker({ position: ffw, map: map });
}
